'use strict';

angular.module('rootCtrl', [])

/**
 * rootCtrl : controller of navigation bar and header alerts
 */
.controller('rootCtrl', function($scope, alertSrv, endpointSrv, APPNAME) {
	
	$scope.appName = APPNAME;
	$scope.jwt = {};
	
    $scope.alerts = alertSrv.alerts;
    
    $scope.removeAlert = function(index) {
    	alertSrv.removeAlert(index);
    }
    
    if (alertSrv.getCount() == 0) alertSrv.addAlert("info", "wait a few seconds for the first verification");
    
	/**
	 * get Example1
	 */
	$scope.getExample1 = function() {
		$scope.jwt.tok = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJpc3N1ZXIiLCJzdWIiOiJzdWJqZWN0IiwiYXVkIjoiYXVkaWVuY2UiLCJleHAiOjAsImlhdCI6MTM5MTcwNTIxM30.FKd6-NwtUJc42fbxxZ2Nx525zXtMPyWoGY2ghKv_zVE";
		$scope.jwt.key = "secretKey";
		$scope.jwt.iss = "issuer";
		$scope.jwt.aud = "audience";
	};
	
	/**
	 * get Example2
	 */
	$scope.getExample2 = function() {
		$scope.jwt.tok = "eyJhbGciOiJub25lIn0.eyJpc3MiOiJqb2UiLA0KICJleHAiOjEzMDA4MTkzODAsDQogImh0dHA6Ly9leGFtcGxlLmNvbS9pc19yb290Ijp0cnVlfQ.";
		$scope.jwt.key = null;
		$scope.jwt.iss = "joe";
		$scope.jwt.aud = null;
	};
	
	/**
	 * verify JWT
	 */
	$scope.verify = function() {
		if($scope.jwt.key=="") delete $scope.jwt.key; 
		if($scope.jwt.iss=="") delete $scope.jwt.iss; 
		if($scope.jwt.aud=="") delete $scope.jwt.aud; 
		endpointSrv.verifyJWT($scope.jwt, function(){
		});
	};


});
