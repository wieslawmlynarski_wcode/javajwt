package com.unblau.javajwt.model;

import java.io.Serializable;

public class Args implements Serializable
{
	private static final long serialVersionUID = 1L;

	private String tok;
	private String key;
	private String iss;
	private String aud;
	
	public Args() {}
	
	public String getTok() {return tok;}
	public void setTok(String tok) {this.tok = tok;}
	
	public String getKey() {return key;}
	public void setKey(String key) {this.key = key;}
	
	public String getIss() {return iss;}
	public void setIss(String iss) {this.iss = iss;}

	public String getAud() {return aud;}
	public void setAud(String aud) {this.aud = aud;}

}