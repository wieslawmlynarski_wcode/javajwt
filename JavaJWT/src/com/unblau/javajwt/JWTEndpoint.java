package com.unblau.javajwt;

import com.google.api.server.spi.config.Api;
import com.google.api.server.spi.config.ApiMethod;
import com.google.api.server.spi.config.ApiMethod.HttpMethod;
import com.google.api.server.spi.config.ApiNamespace;
import com.unblau.javajwt.model.Args;

@Api(
	name = "javaJWT",
	description = "Endpoints of JavaJWT App",
	version = "v1",
	namespace = @ApiNamespace(ownerDomain = "unblau.com", ownerName = "unblau.com", packagePath = "javajwt")
)
public class JWTEndpoint
{
	@ApiMethod(name = "verify", httpMethod = HttpMethod.POST, path = "verify")
	public void verify(Args args) throws Exception
	{		
		JWT jwt = new JWT();
		jwt.verify(args);
	}	
}
